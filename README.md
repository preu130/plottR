# plottR
A small shiny app for plotting sequences. I came up with this after we had to do a lot of exercises on limits of sequences in my math introductory course and I got a bit fed up with wolframalpha not letting me use the "premium" features such as zooming. 
This is a beta version! You can check it out live at <https://friep.shinyapps.io/plottr/> or from within R using runGitHub (see <http://shiny.rstudio.com/tutorial/lesson7/> for details).
