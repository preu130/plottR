library(plotly)

shinyServer(function(input, output) {
  source("plotseq.R")
  
  df_list <- eventReactive(input$submit, {
    validate(
      need(is.integer(input$lowerx) == TRUE, "Please enter a number from the set of natural numbers as starting index."),
      need(input$lowerx >= 0, "Please enter a number from the set of natural numbers as starting index."),
      
      need(input$upperx >= 0, "Please enter a number from the set of natural numbers as final index."),
      need(is.integer(input$upperx) == TRUE, "Please enter a number from the set of natural numbers as final index."),
      need(input$upperx > input$lowerx, "Final index cannot be smaller than starting index."),
      
      need((input$seq1 != "" | input$seq2 != "" | input$seq3 != "" | input$seq4 != ""), 
           "Please enter at least one sequence.")
    )
    
    seq_strings <- c(seq1 = input$seq1, seq2 = input$seq2, seq3 = input$seq3, seq4 = input$seq4)
    seq_strings <- seq_strings[nchar(seq_strings)>0]
    
    seq_list <- build_seq_list(seq_strings)
    df_list <- calc_seqs(seq_list, input$lowerx, input$upperx)
  })
  
  p <- eventReactive(input$submit, {
    p <- plot_seq(df_list()[["df_long"]])
  })
  
  output$outputdata <- renderTable(
    df <- df_list()[["df_wide"]]
  )
  
  output$plot <- renderPlotly({
    if(input$coloursens){
      p <- p() +
        scale_color_brewer(palette = "Set2")
    }else{
      p <- p() +
        scale_color_brewer(palette = "Set1")
    }
  
    if(input$addlines){
      p <- p +
      geom_line(size = 0.1)
    }
    
    ggplotly(p)
  })
  
  
  
})

#make more efficient: only calc. sequences that changed 